using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    [SerializeField]
    Transform[] _spawnPositions;

    [SerializeField]
    GameObject[] _animalPrefabs;



    public event Action<GameObject> OnAnimalInstantiated;
    


    public void SpawnAnimal()
    {
        GameObject animalPrefab = _animalPrefabs[UnityEngine.Random.Range(0, _animalPrefabs.Length)];
        Quaternion rotation = Quaternion.Euler(0.0f, UnityEngine.Random.Range(0.0f, 360.0f), 0.0f);
        Transform parent = _spawnPositions[UnityEngine.Random.Range(0, _spawnPositions.Length)];

        var animal = Instantiate(animalPrefab, parent.transform.position, rotation, parent);
        OnAnimalInstantiated?.Invoke(animal);
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class Animal : MonoBehaviour
{
    [SerializeField]
    GameObject _uiPanel;

    private Rigidbody _rb;

    private float _startTime = 0f;
    private bool _isPrey = false;


    public Rigidbody Rigidbody
    {
        get { return _rb; }
    }

    public float StartTime
    { 
        get { return _startTime; } 
    }

    public bool IsPrey
    {
        get { return _isPrey; }
    }

    public GameObject UIPanel
    {
        get 
        {  
            Debug.Assert(_uiPanel != null);
            return _uiPanel;
        }
    }



    private void Start()
    {
        _rb = GetComponent<Rigidbody>();
        _startTime = Time.time;
        _isPrey = gameObject.tag.Equals(Tags.Prey);
    }
}

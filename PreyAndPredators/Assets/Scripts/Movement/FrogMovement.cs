using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FrogMovement : PeriodicalMovement
{
    [SerializeField]
    float _jumpingForce = 5f;


    protected override void Move()
    {
        _animal.Rigidbody.velocity = (transform.forward + transform.up) * _jumpingForce;
    }
}

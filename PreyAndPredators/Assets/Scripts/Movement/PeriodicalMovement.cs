using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PeriodicalMovement : MonoBehaviour
{
    [SerializeField]
    float _repeatRate = 2.0f;

    protected Animal _animal;

    private float _startMovingTime = 1.0f;



    protected void Start()
    {
        _animal = GetComponent<Animal>();
        InvokeRepeating("Move", _startMovingTime, _repeatRate);
    }

    protected virtual void Move()
    {
        throw new System.NotImplementedException();
    }
}

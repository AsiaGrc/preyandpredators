using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class SnakeMovement : MonoBehaviour
{
    [SerializeField]
    float speed = 2.0f;

    private void FixedUpdate()
    {
        transform.position += transform.forward * Time.deltaTime * speed;
    }
}

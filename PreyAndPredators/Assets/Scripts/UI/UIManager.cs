using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class UIManager : MonoBehaviour
{
    [SerializeField]
    TextMeshProUGUI _predatorDead;

    [SerializeField]
    TextMeshProUGUI _preyDead;


    public void ChangePredatorsScore(int count)
    {
        _predatorDead.text = count.ToString();
    }

    public void ChangePreyScore(int count)
    {
        _preyDead.text = count.ToString();
    }

    public void DisplayPanel_Tasty(GameObject panel)
    {
        StartCoroutine(UIPanelEnable(panel));
    }

    private IEnumerator UIPanelEnable(GameObject panel)
    {
        panel.SetActive(true);
        yield return new WaitForSeconds(1f);
        if (panel != null) panel.SetActive(false);
    }
}

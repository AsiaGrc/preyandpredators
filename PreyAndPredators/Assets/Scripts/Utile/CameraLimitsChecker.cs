using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraLimitsChecker : MonoBehaviour
{
    [SerializeField]
    Transform target;

    [SerializeField]
    float turnSpeed = 0.1f;

    Vector3 direction;
    Quaternion rotationToTarget;

    Camera _mainCamera;
    Collider _collider;
    Plane[] _planes;


    private void Start()
    {
        _mainCamera = Camera.main;
        _planes = GeometryUtility.CalculateFrustumPlanes(_mainCamera);
        _collider = GetComponent<Collider>();
    }

    private void Update()
    {
        if (!GeometryUtility.TestPlanesAABB(_planes, _collider.bounds))
        {
            direction = (target.position - transform.position).normalized;
            rotationToTarget = Quaternion.LookRotation(direction);
            transform.rotation = Quaternion.Slerp(transform.rotation, rotationToTarget, turnSpeed);
        }
    }
}

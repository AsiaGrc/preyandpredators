﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    [SerializeField]
    Spawner _spawner;
    [SerializeField]
    UIManager _uiManager;


    int _deadPredatorsCount = 0;
    int _deadPreysCount = 0;

    float _spawnRepeatRate = 2f;
    float _spawnStartInvokingTime = 2f;


    private void Awake()
    {
        _spawner.OnAnimalInstantiated += SubscribeOnDeath;
    }

    private void Start()
    {
        InvokeRepeating("SpawnAnimals", _spawnStartInvokingTime, _spawnRepeatRate);
    }

 

    private void SubscribeOnDeath(GameObject animal)
    {
        var colManager = animal.GetComponent<CollisionManager>();
        colManager.OnPreyDead += GameManager_OnPreyDead;
        colManager.OnPredatorDead += GameManager_OnPredatorDead;
    }

    private void GameManager_OnPredatorDead(GameObject animal)
    {
        DisplayPanel_Tasty(animal);
        _uiManager.ChangePredatorsScore(++_deadPredatorsCount);
    }

    private void GameManager_OnPreyDead(GameObject animal)
    {
        DisplayPanel_Tasty(animal);
        _uiManager.ChangePreyScore(++_deadPreysCount);
    }

    private void DisplayPanel_Tasty(GameObject animal)
    {
        GameObject panel = animal.GetComponent<Animal>().UIPanel;
        if (panel == null) return;
        _uiManager.DisplayPanel_Tasty(panel);
    }

    private void SpawnAnimals() => _spawner.SpawnAnimal();
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Tags
{
    public const string WallTag = "Wall";
    public const string Predator = "Predator";
    public const string Prey = "Prey";
}

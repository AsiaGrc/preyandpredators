using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotationPositionConstraint : MonoBehaviour
{
    GameObject _parent;
    Vector3 _posOffset;


    private void Start()
    {
        _parent = transform.parent.gameObject;
        _posOffset = transform.localPosition;
    }

    void Update()
    {
        transform.rotation = Quaternion.Euler(90f, -_parent.transform.rotation.y, 0f);
        transform.position = _parent.transform.position + _posOffset;
    }
}

﻿using System;
using UnityEngine;


public class CollisionManager : MonoBehaviour
{
    private Animal _animal;


    public event Action<GameObject> OnPredatorDead;
    public event Action<GameObject> OnPreyDead;

    float _repulsiveForce = 10f;

    private void Start()
    {
        _animal = GetComponent<Animal>();
    }

    private void OnCollisionEnter(Collision collision)
    {
        var collisionObj = collision.gameObject;
        if (collisionObj.tag.Equals(Tags.WallTag))
        {
            transform.rotation *= Quaternion.Euler(0f, UnityEngine.Random.Range(90f, 120f), 0f);
        }
        else if (collisionObj.tag.Equals(Tags.Prey))
        {
            if (_animal.IsPrey)
            {
                _animal.Rigidbody.velocity = -transform.forward * _repulsiveForce;
            }
        }
        else if (collisionObj.tag.Equals(Tags.Predator))
        {
            if (_animal.IsPrey)
            {
                OnPreyDead?.Invoke(collisionObj);
                Destroy(gameObject);
            }
            else
            {
                var thisAnimalLivingTime = Time.time - _animal.StartTime;
                var colAnimalLivingTime = Time.time - collision.gameObject.GetComponent<Animal>().StartTime;
                if (thisAnimalLivingTime < colAnimalLivingTime)
                {
                    OnPredatorDead?.Invoke(collisionObj);
                    Destroy(gameObject);
                }
            }
        }
    }
}


